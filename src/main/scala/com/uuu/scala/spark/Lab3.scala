package com.uuu.scala.spark

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

object Lab3 {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)
    // start connect to local
    val config = new SparkConf().setAppName("My simple app").setMaster("local[*]")
    // get sc
    val sc = new SparkContext(config)
    val textFile1 = sc.textFile("yarn.cmd")
    val result1 = textFile1.collect()
    println("result1=", result1);
    val result2 = textFile1.collect().mkString("<",",",">\n")
    println("result2=", result2)
    val result3 = textFile1.collect().filter(line=>line.contains("a")).mkString("<",",",">\n")
    println("result3=", result3)
    textFile1.collect.filter(line=>line.contains("b")).foreach(println)
    textFile1.collect.filter(line=>line.contains("c")).foreach(line=>print(line.length+"{"+line+"}\n"))
    textFile1.collect.filter(line=>line.contains("d"))
      .foreach(print)
    println("now CAPATIALIZE")
    textFile1.collect
      .filter(line=>line.contains("d"))
      .map(_.toUpperCase)
      .foreach(print)
    sc.stop()
  }
}
