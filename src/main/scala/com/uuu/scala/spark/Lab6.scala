package com.uuu.scala.spark

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

object Lab6 {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)
    // start connect to local
    val config = new SparkConf().setAppName("My simple app").setMaster("local[*]")
    // get sc
    val sc = new SparkContext(config)
    val rdd2 = sc.makeRDD(Array(("A", 1), ("B", 2), ("C", 3), ("D", 4), ("E", 5)))
    val result = rdd2.reduce((x, y) => {
      (x._1 + y._1
        , x._2 + y._2)
    })
    val result2 = rdd2.reduce((x, y) => {
      (x._1 + y._1
        , x._2 - y._2)
    })
    println(rdd2.collect().mkString("{",",","}"))
    println(result)
    println(result2)
    sc.stop()
  }
}