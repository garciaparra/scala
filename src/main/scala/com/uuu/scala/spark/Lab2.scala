package com.uuu.scala.spark

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

object Lab2 {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)
    // start connect to local
    val config = new SparkConf().setAppName("My simple app").setMaster("local[*]")
    // get sc
    val sc = new SparkContext(config)
    val textFile1 = sc.textFile("yarn.cmd")
    val result = textFile1.count()
    println("\nLines with yarn.cmd=%d".format(result))
    val numAs = textFile1.filter(line => line.contains("a")).count()
    val numBs = textFile1.filter(line => line.contains("b")).count()
    val numCs = textFile1.filter(line => line.contains("c")).count()
    println(s"lines with a: $numAs, b: $numBs, c: $numCs")
    sc.stop()
  }
}