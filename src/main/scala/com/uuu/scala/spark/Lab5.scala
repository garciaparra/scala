package com.uuu.scala.spark

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

object Lab5 {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)
    // start connect to local
    val config = new SparkConf().setAppName("My simple app").setMaster("local[*]")
    // get sc
    val sc = new SparkContext(config)
    val rdd1 = sc.makeRDD(1 to 10)
    println("what is rdd1?", rdd1.collect().mkString("{", ",", "}\n"))
    println(rdd1.reduce(_ + _))
    println(rdd1.reduce((a, b) => {
      a + b
    }))
    println(rdd1.reduce(_ - _))
    println(rdd1.reduce((a, b) => {
      a - b
    }))
    sc.stop()
  }
}