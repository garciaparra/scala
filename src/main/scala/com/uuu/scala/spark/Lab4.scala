package com.uuu.scala.spark

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

object Lab4 {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)
    // start connect to local
    val config = new SparkConf().setAppName("My simple app").setMaster("local[*]")
    // get sc
    val sc = new SparkContext(config)
    val textFile1 = sc.textFile("yarn.cmd")
    val result1 = textFile1.map(line => line.split("\\s+"))
    val result3 = textFile1.map(line => line.split("\\s+"))
    val result2 = textFile1.flatMap(line => line.split("\\s+"))
    println(result1.collect.mkString("{", ",", "}\n"))
    // result3.collect.foreach()
    println(result3.collect.foreach(e => println(e.mkString("{", ",", "}\n"))))
    println(result2.collect.mkString("{", ",", "}\n"))
    sc.stop()
  }
}